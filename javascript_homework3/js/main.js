
// Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
// - Цикли потрібні для того, щоб оптимізувати код (зробити коротшим) при необхідності його повторення декілька разів в різних місцях.

// Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
// - Цикл "while" використовується, коли є необхідність перевірити вказані умови перед виконанням функції/ Наприклад, необхідно перевірити правильність введеної інформації, а далі вже виконувати якусь із функцій. А "do...while" - спочатку виконує функцію, а потім перевіряє її умови. 

// Що таке явне та неявне приведення (перетворення) типів даних у JS?
// - Явне перетвонення виконується за допомогою відповідних операторів, наприклад Number(value). Неявне приведення може виконуватись при використанні нестрогого дорівнює (==).


let num = Number(prompt("Add your number"));

while (!Number.isInteger (num)) {
    num = Number(prompt("Error! Must be an integer! Add your number"));
}

for (let i = 0; i <= num; i++) {
    if (i % 5 === 0 && i !== 0) {
        console.log(i);
    } 
    if (num <= 4) {
        console.log("Sorry, no numbers");
        break;
    }
}