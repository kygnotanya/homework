// Опишіть своїми словами, що таке метод об'єкту
// - Метод об'єкту дозволяє зберігати в собі структуровану інформацію та функції.

// Який тип даних може мати значення властивості об'єкта?
// - Всі дочерні об'єкти успадковують методи і властивості материнського.

// Об'єкт це посилальний тип даних. Що означає це поняття?
// - Тобто посилальний тип є посилання на інше значення.



// Завдання
// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
// Технічні вимоги:
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.
// Необов'язкове завдання підвищеної складності
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та 
// setLastName(), які дозволять змінити дані властивості.


const createNewUser = () => {
    let firstName = 'Tetiana';
    let lastName = 'Kuhno';
        return Object.create( {}, {
            firstName: {
                value: firstName,
                enumerable: true,
                writable: false,
            },
            lastName: {
                value: lastName,
                enumerable: true,
                writable: false,
            },
            setFirstName(value) {
                Object.defineProperty(this, firstName, {
                    writable:true,
                });
                firstName = value;
            },
            setLastName(value) {
                Object.defineProperty(newUser, lastName, {
                    writable:true,
                });
                lastName = value;
            },
            getLogin() {
                let result = firstName[0] + lastName;
                return result.toLowerCase();
            },
        })
}

const newUser = createNewUser();

console.log(newUser);

console.log(newUser.getLogin());


newUser.setFirstName("Tati");
newUser.setLastName("Kuhnova");
console.log(newUser);

newUser.firstName = 'Nadiia';
newUser.lastName = 'Ruzhuk';
console.log(newUser);